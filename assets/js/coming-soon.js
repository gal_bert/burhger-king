var canvas = document.getElementById("canvas")
var ctx = canvas.getContext("2d")
var img = new Image()
img.src = "./assets/images/burger.png"

var reverse = false
var imgWidth
var imgHeight

var i = 0
var j = 0

img.onload = function () {
    
    setInterval(() => {
        if (window.matchMedia("(min-width: 991px)").matches){
            imgWidth = img.width/8
            imgHeight = img.height/4
        } else {
            imgWidth = canvas.width/8
            imgHeight = canvas.height/4
        }

        ctx.clearRect(0, 0, canvas.width, canvas.height)
        ctx.drawImage(img, i, j, imgWidth, imgHeight)
        
        if (j == canvas.height - 80){
            reverse = true
        } else if (j == 0){
            reverse = false
        }
        
        if(!reverse){
            i += 2
            j += 0.5
        } else {
            i-= 2
            j-= 0.5
        }
    }, 10)
}

