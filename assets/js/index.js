let imageCount = 3
var i = 1

var containerWidth = $(".slider-container").width()

$(document).ready(function() {
    $(".slider-item").width(containerWidth)
})

setInterval(() => {

    let left = $(".slider-content-wrapper").css("left").split("px")[0]

    if (i == imageCount) {
        i = 0
        $(".slider-content-wrapper").animate({
            left: 0
        })
    }
    else {
        $(".slider-content-wrapper").animate({
            left: left - containerWidth
        })
    }
    
    i++

}, 2000);