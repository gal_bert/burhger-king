document.getElementById("order-form").addEventListener("submit", function(e) {
    var fullname = document.getElementById("fullname").value
    var email = document.getElementById("email").value
    var selectedBurger = document.getElementById("selected-burgers").value
    var qty = document.getElementById("quantity").value
    var address = document.getElementById("address").value
    var checkbox = document.getElementById("checkbox").checked

    if (fullname == "" || email == "" || selectedBurger == "" || qty == "" || address == "" || !checkbox) {
        $('#alertModal').modal('show')
        $('#modal-body').html("Please fill all the fields and agree to our terms and conditions!")
        e.preventDefault()
    }

    else if (!fullname.includes(" ")) {
        $('#alertModal').modal('show')
        $('#modal-body').html("Name must contain minimum 2 words!")
        e.preventDefault()
    }

    else if (!email.endsWith("@gmail.com") || email.endsWith("@yahoo.com") || email.endsWith("@hotmail.com")) {
        $('#alertModal').modal('show')
        $('#modal-body').html("Email used must be gmail, yahoo, or hotmail!")
        e.preventDefault()
    }

    else if (qty < 1){
        $('#alertModal').modal('show')
        $('#modal-body').html("Quantity must be greater than 0!")
        e.preventDefault()
    }

    else if (address.length < 10) {
        $('#alertModal').modal('show')
        $('#modal-body').html("Address must be 10 characters or more!")
        e.preventDefault()
    }

});